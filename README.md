Frp-Server
===========
1.安装frps  
cd /usr/local  
wget https://gitee.com/cilee/frps/raw/master/frp_0.29.0_linux_amd64.tar.gz  
tar -xzvf frp_0.29.0_linux_amd64.tar.gz  
mkdir frps  
cd frp_0.29.0_linux_amd64  
cp frps  frps.ini ../frps  
cd /usr/local  
rm -rf  frp_0.29.0_linux_amd64 frp_0.29.0_linux_amd64.tar.gz  

nano /usr/local/frps/frps.ini  
[common]  
bind_port = 1077  
log_file = ./frps.log  
log_level = info  
log_max_days = 3  
token = kgxxxxxxxx  
allow_ports = 2011,1433  
max_pool_count = 50  
max_ports_per_client = 0  
tcp_mux = true  

2.配置systemctl来控制frps  
  创建frps.service ,nano /lib/systemd/system/frps.service  
[Unit]  
Description=frps service  
After=network.target syslog.target  
Wants=network.target  

[Service]  
Type=simple  
ExecStart=/usr/local/frps/frps -c /usr/local/frps/frps.ini  

[Install]  
WantedBy=multi-user.target  

开机自动启动frps  
systemctl enable frps  

systemctl start/stop/restart/status frps
